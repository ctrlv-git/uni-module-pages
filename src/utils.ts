import { createWriteStream } from "fs";

export function normalizeUniModulesPagesJson(
  pagesJson: { pages: any[]; subPackages: any[] },
  pluginId: string
) {
  if (Array.isArray(pagesJson.pages)) {
    pagesJson.pages.forEach((page: { path: string }) => {
      page.path = "uni_modules/" + pluginId + "/" + page.path;
    });
  }
  if (Array.isArray(pagesJson.subPackages)) {
    pagesJson.subPackages.forEach((subPackage: { root: string }) => {
      subPackage.root = "uni_modules/" + pluginId + "/" + subPackage.root;
    });
  }
  return pagesJson;
}

export function logger(data: any, path = "log.stdout.log") {
  const options = {
    flags: "w",
    encoding: "utf8" as BufferEncoding,
  };
  const stderr = createWriteStream(path, options);
  const myConsole = new console.Console(stderr);
  myConsole.log(data);
}
