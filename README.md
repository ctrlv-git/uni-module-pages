# `@ctrlc/uni-module-pages`

> `@ctrlc/uni-module-pages`是基于`uniapp`项目处理`pages.json`模块化的路由插件，可用于将`pages.json`文件中`pages`属性和`subPackages`属性拆分出不同的文件管理。（支持对`uni_modules`插件的路由页面管理）

## 使用手册

仓库迁移至 [WXIM](https://gitee.com/WXIM/packages/tree/master/packages/uni-module-pages)
