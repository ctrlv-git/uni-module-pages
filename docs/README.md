# 示例参考

1. 新建`pages.js`文件(`pages.json`文件同级)，同级新建路由目录（`router`）。

   ```js
   const UniModuleRouter = require("@ctrlc/uni-module-pages");
   const uniRouter = new UniModuleRouter({
     dir: "./router",
     // uni_modules插件需要引入路由
     uniModules: [
       {
         // id:插件名称,
         id: "uni-demo",
         // path:相对插件根路径的定义的路由文件路径
         path: "router.json",
       },
     ],
   });

   module.exports = (pagesJson, loader) => {
     return uniRouter.loader(pagesJson, loader);
   };
   ```

2. 改造项目的`pages.json`文件

   删除`pages`属性和`subPackages`属性中需要迁移的路由信息，（也可保留`pages`属性中入口文件信息）。

   ```js
   // 原 pages.json 文件
   {
     /** 删除
     "pages": [
       {
         "path": "pages/home/home",
         "style": {
           "navigationBarTitleText": "home"
         }
       },
       {
         "path": "pages/serve/index",
         "style": {
           "navigationBarTitleText": "serve"
         }
       }
     ],
     "subPackages": [{
        "root": "pages/subpages",
        "pages": [{
          "path": "index",
          "style": {
            "navigationBarTitleText": "subPackages",
          }
        }]
     }],
     */
     "globalStyle": {
       "navigationBarTextStyle": "white",
       "navigationBarTitleText": "uni-app",
       "backgroundColor": "#F8F8F8"
     },
     "easycom": {
       "autoscan": true
     }
   }
   ```

3. 路由目录`router`下新建`home.json`,`serve.json` 文件

   ```bash
    └─router # 项目路由目录
      ├─home.json # 路由文件
      ├─serve.json # 路由文件
   ```

   ```js
   // home.json
   {
     "pages": [
       {
         "path": "pages/home/home",
         "style": {
           "navigationBarTitleText": "home"
         }
       },
     ]
   }
   // serve.json
   {
     "pages": [
       {
         "path": "pages/serve/index",
         "style": {
           "navigationBarTitleText": "serve"
         }
       }
     ]
     "subPackages": [{
        "root": "pages/subpages",
        "pages": [{
          "path": "index",
          "style": {
            "navigationBarTitleText": "subPackages",
          }
        }]
     }],     
   }

   ```

4. `uni_modules`插件路由(在`uniModules`属性中定义)

   管理`uni_modules`插件`sub-demo`中需要添加的页面

   ```js
   // router.json
   {
     "subPackages": [
       {
         // 以插件的根目录定义路径
         "root": "pages",
         "pages": [
           {
             "path": "sub-demo/sub-demo",
             "style": {
               "navigationBarTitleText": "sub-demo"
             }
           }
         ]
       }
     ]
   }
   ```
